//
// Created by Mikulas Muron on 04/05/2020.
//

#ifndef ZOO_CV_COMMAND_PRIKAZ_H
#define ZOO_CV_COMMAND_PRIKAZ_H


#include "Lektvar.h"
#include "Hrdina.h"
#include <iostream>

class Prikaz {
public:
    virtual void pouzijLektvar(Lektvar* lektvar, Hrdina* hrdina) = 0;
    virtual std::string getPopis() = 0;
};


#endif //ZOO_CV_COMMAND_PRIKAZ_H
