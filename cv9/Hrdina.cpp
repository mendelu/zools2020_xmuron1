#include "Hrdina.h"
#include "Prikaz.h"

Hrdina::Hrdina(int zivot) {
    m_zivot = zivot;
}

void Hrdina::vypisLektvary() {
    int i = 0;
    for(Lektvar* lektvar : m_lektvary){
        cout << i << ". " << lektvar->getPopis() << endl;
        i++;
    }
}


void Hrdina::vypisPrikazy() {
    for(int i=0;i<m_prikazy.size();i++){
        cout << i << ". " << m_prikazy.at(i)->getPopis() << endl;
    }
}

void Hrdina::seberLektvar(Lektvar* jaky) {
    m_lektvary.push_back(jaky);
}


int Hrdina::getZivot() {
    return m_zivot;
}

void Hrdina::zvysZivot(int kolik) {
    m_zivot += kolik;
}

void Hrdina::naucSePrikaz(Prikaz *jaky) {
    m_prikazy.push_back(jaky);
}

void Hrdina::pouzijLektvar() {

    int index_lektvaru = 0;
    vypisLektvary();
    cin >> index_lektvaru;
    // TODO kontrola rozsahu indexu

    int index_prikazu = 0;
    vypisPrikazy();
    cin >> index_prikazu;
    // TODO kontrola rozsahu indexu

    Lektvar* lektvar = m_lektvary.at(index_lektvaru);
    m_prikazy.at(index_prikazu)->pouzijLektvar(lektvar,this);

}
