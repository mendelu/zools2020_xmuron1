cmake_minimum_required(VERSION 3.15)
project(zoo_cv_command)

set(CMAKE_CXX_STANDARD 14)

add_executable(zoo_cv_command main.cpp Hrdina.cpp Hrdina.h Lektvar.cpp Lektvar.h Prikaz.h PrikazVypijVse.cpp PrikazVypijVse.h Prikaz.cpp PrikazUpijTrochu.cpp PrikazUpijTrochu.h PrikazProhledni.cpp PrikazProhledni.h)