#include <iostream>
#include "Hrdina.h"
#include "Lektvar.h"
#include "PrikazUpijTrochu.h"
#include "PrikazVypijVse.h"
#include "PrikazProhledni.h"

int main() {
    Hrdina* h = new Hrdina(100);

    Lektvar* l1 = new Lektvar(5,"Pivo");
    Lektvar* l2 = new Lektvar(10,"Elixir");
    h->seberLektvar(l1);
    h->seberLektvar(l2);

    Prikaz* upij_trochu = new PrikazUpijTrochu();
    Prikaz* vypij_vse = new PrikazVypijVse();
    Prikaz* prohledni = new PrikazProhledni();
    h->naucSePrikaz(upij_trochu);
    h->naucSePrikaz(vypij_vse);
    h->naucSePrikaz(prohledni);

    h->pouzijLektvar();

    return 0;
}