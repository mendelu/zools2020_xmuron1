//
// Created by Mikulas Muron on 04/05/2020.
//

#include "PrikazUpijTrochu.h"


void PrikazUpijTrochu::pouzijLektvar(Lektvar *lektvar, Hrdina *hrdina) {
    int bonus = lektvar->getBonusZivot()/2;
    hrdina->zvysZivot(bonus);
    lektvar->setBonusZivot(bonus);

    // debug vypis
    cout << "Upil si trochu lektvaru s bonusem " << bonus << ". Mas ted " << hrdina->getZivot() << " zivotu" << endl;
}

std::string PrikazUpijTrochu::getPopis() {
    return "upij pulku lektvaru";
}