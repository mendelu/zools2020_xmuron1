//
// Created by Mikulas Muron on 04/05/2020.
//

#include "PrikazVypijVse.h"


void PrikazVypijVse::pouzijLektvar(Lektvar *lektvar, Hrdina *hrdina) {
    int bonus = lektvar->getBonusZivot();
    hrdina->zvysZivot(bonus);
    lektvar->setBonusZivot(0);

    // debug vypis
    cout << "Vypil jsi cely lektvar s bonusem " << bonus << ". Mas ted " << hrdina->getZivot() << " zivotu" << endl;
}

std::string PrikazVypijVse::getPopis() {
    return "vypij cely lektvar";
}