//
// Created by Mikulas Muron on 04/05/2020.
//

#ifndef ZOO_CV_COMMAND_PRIKAZPROHLEDNI_H
#define ZOO_CV_COMMAND_PRIKAZPROHLEDNI_H

#include "Prikaz.h"

class PrikazProhledni : public Prikaz {
public:
    void pouzijLektvar(Lektvar* lektvar, Hrdina* hrdina);
    std::string getPopis();
};


#endif //ZOO_CV_COMMAND_PRIKAZPROHLEDNI_H
