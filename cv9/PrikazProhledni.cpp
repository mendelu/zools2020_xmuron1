//
// Created by Mikulas Muron on 04/05/2020.
//

#include "PrikazProhledni.h"

void PrikazProhledni::pouzijLektvar(Lektvar *lektvar, Hrdina *hrdina) {
    cout << lektvar->getPopis() << " dava bonus "<< lektvar->getBonusZivot() << endl;
}

std::string PrikazProhledni::getPopis() {
    return "zjisti bonus lektvaru";
}