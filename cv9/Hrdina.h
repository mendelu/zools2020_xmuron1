#ifndef HRDINA_H
#define HRDINA_H
#include <iostream>
#include <vector>
#include "Lektvar.h"



using namespace std;

class Prikaz;

class Hrdina {

private:
    int m_zivot;
    vector<Lektvar*> m_lektvary;
    vector<Prikaz*> m_prikazy;

    void vypisPrikazy();
    void vypisLektvary();

public:
    Hrdina(int zivot);
    void seberLektvar(Lektvar* jaky);
    void naucSePrikaz(Prikaz* jaky);
    int getZivot();
    void zvysZivot(int kolik);
    void pouzijLektvar();
};
#endif // HRDINA_H


