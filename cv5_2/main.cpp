#include <iostream>
using namespace std;

class Baterie{

private:
    int m_stav;

    void korigujStav(){
        if(m_stav < 0){
            m_stav = 0;
        }
        if(m_stav > 100){
            m_stav = 100;
        }

    }
public:
    Baterie(){
        m_stav = 100;
    }

    int getStav(){
        return m_stav;
    }

    void odectiOdStavu(int kolik_procent){
        // TODO pridat dalsi kontroly
        m_stav -= kolik_procent;
        korigujStav();

    }

    void prictiKeStavu(int kolik_procent){
        // TODO pridat dalsi kontroly
        m_stav += kolik_procent;
        korigujStav();
    }



};

class Elektromobil{
private:
    string m_jmeno_vyrobce;
    Baterie* m_baterie;
    int m_spotreba;

    bool mamBaterii(){
        if(m_baterie == nullptr){
            return false;
        }
        else{
            return true;
        }
    }

public:
    Elektromobil(string vyrobce, int spotreba){
        m_jmeno_vyrobce = vyrobce;
        m_spotreba = spotreba;
        // chceme vytvorit automobil i s baterii nebo bez?
        // varianta 1
        // m_baterie = nullptr;
        // varianta 2
        m_baterie = new Baterie();
    }

    void vymenBaterii(Baterie* baterie){
        m_baterie = baterie;
    }

    void odeberBaterii(){
        m_baterie = nullptr;
    }

    int getStavBaterie(){
        if(m_baterie == nullptr){
            return 0;
        }
        else {
            return m_baterie->getStav();
        }
    }

    float getDojezd(){
        return (getStavBaterie()/m_spotreba) * 100;
    }

    void jezdi(int pocet_km){
        float spotreba_baterie = (m_spotreba / 100.0) * pocet_km;
        if(spotreba_baterie > getStavBaterie()){
            cout << "Ta takovou vzdalenost nemas baterku!" << endl;
        }
        else {
            m_baterie->odectiOdStavu(spotreba_baterie);
        }
    }

    void printInfo(){
        cout << "Jsem elektromobil vyrobce" << m_jmeno_vyrobce << endl;
        cout << "Mam spotrebu " << m_spotreba << " na 100 km" << endl;
        cout << "Aktualni kapacita baterie je " << getStavBaterie() << endl;
        cout << "Dojezd je " << getDojezd() << endl;
    }

    void pridejKStavuBaterie(int kolik_procent){
        if(mamBaterii()){
            m_baterie->prictiKeStavu(kolik_procent);
        }
        else{
            cout << "Nemas v aute baterii, nemuzu nic nabijet!" << endl;
        }
    }


};

class NabijeciStanice{
private:
    int m_rychlost_nabijeni;
    int m_celkove_nabito;
public:
    NabijeciStanice(int rychlost_nabijeni){
       m_rychlost_nabijeni = rychlost_nabijeni;
       m_celkove_nabito = 0;
    }

    void nabij(int doba_nabijeni,Elektromobil* elektromobil){
        int kolik_procent = doba_nabijeni * m_rychlost_nabijeni;
        elektromobil->pridejKStavuBaterie(kolik_procent);
    }
};



int main() {
    Baterie* b1 = new Baterie();
    Baterie* b2 = new Baterie();

    Elektromobil* elektro1 = new Elektromobil("Tesla",20);
    elektro1->vymenBaterii(b1);

    cout << elektro1->getDojezd() << endl;
    elektro1->jezdi(100);

    elektro1->odeberBaterii();
    elektro1->vymenBaterii(b2);

    NabijeciStanice* stanice = new NabijeciStanice(10);
    stanice->nabij(10,elektro1);

    cout << elektro1->getDojezd() << endl;

    return 0;
}