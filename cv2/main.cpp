#include <iostream>

using namespace std;

class Parcel {
public:
    string m_sender;
    string m_receiver;
    float m_weight;
    int m_cashOnDelivery;

    // KONSTUKTOR MUSI INICIALIZOVAT VSETKY ATRIBUTY!
    Parcel(string sender, string receiver) {
        // sender = m_sender ZLE!!!!
        m_sender = sender; // KOMENTAR
        m_receiver = receiver;
        m_weight = 0.0;
        m_cashOnDelivery = 0;
    }

    Parcel(string sender, string receiver, float weight) {
        m_sender = sender;
        m_receiver = receiver;
        m_weight = weight;
        m_cashOnDelivery = 0;
    }

    Parcel(string sender, string receiver, int cashOnDelivery) {
        m_sender = sender;
        m_receiver = receiver;
        m_weight = 0.0;
        m_cashOnDelivery = cashOnDelivery;
    }

    Parcel(string sender, string receiver, float weight,
           int cashOnDelivery) {
        m_sender = sender;
        m_receiver = receiver;
        m_weight = weight;
        m_cashOnDelivery = cashOnDelivery;
    }

    void setWeight(float newWeight) {
        if (newWeight < 0) {
            // podmienka plati
            // vaha je nevalidna
            cout << "Nelze mit zapornou vahu!!!";
            m_weight = 0.0;
        } else {
            // podmienka neplati - vaha je vacsia/rovna ako 0
            m_weight = newWeight;
        }
    }

    void replaceReceiverBySender() {
        m_receiver = m_sender;
    }

    void printInfo() {
        cout << "sender: " << m_sender << endl;
        cout << "receiver: " << m_receiver << endl;
        cout << "weight: " << m_weight << endl;
        cout << "cash on delivery: " << m_cashOnDelivery << endl;
    }


};


int main() {
    Parcel *toTomas = new Parcel("Petr", "Tomas",
                                 50.5, 2000);
    toTomas->printInfo();

    cout << "-------------------------" << endl;

    // nahrada prijemcu za odosielatela
    toTomas->replaceReceiverBySender();
    toTomas->printInfo();

    delete toTomas;
    // delete(toTomas);
    return 0;
}