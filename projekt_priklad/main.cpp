#include <iostream>
#include <vector>
#include "Mapa.h"

int main() {
    // TODO Presunout do zvlastni tridy. V mainu nic nebude!

    Mapa* mapa = Mapa::getMapa();
    // TODO Zatim vytvorime zvirata rucne. Predelat na tovarnu
    mapa->ulozNaPozici(1,0,new Zvire("Zvire 1",1));
    mapa->ulozNaPozici(2,0,new Zvire("Zvire 2",2));
    mapa->ulozNaPozici(3,0,new Zvire("Zvire 3",2));
    mapa->ulozNaPozici(4,0,new Zvire("Zvire 4",1));

    // odehrajeme par kol
    int pocet_kol = 20;
    for(int i=0;i<pocet_kol;i++){
        mapa->vypisMapu();
        mapa->pohybZvirat();
        mapa->interakceZvirat();
    }
    return 0;
}