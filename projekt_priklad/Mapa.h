//
// Created by Mikulas Muron on 08/04/2020.
//
#ifndef PROJEKT_MAPA_H
#define PROJEKT_MAPA_H

#include <array>
#include <vector>
#include <iostream>
#include "PoleMapy.h"

using std::cout;
using std::endl;

class Mapa {
private:
    // vytvorime hraci plochu (2d array) 5x5 policek
    std::array<std::array<PoleMapy*,5>,5> m_mapa;
    // Využijeme NV singleton
    static Mapa* s_mapa; // tady si budeme uchovávat instanci
    // Konstruktor. zvenku nikdo nevytvori instanci pres new
    Mapa();
    // TODO destruktor

    bool jeValidniSouradnice(int x, int y);
    std::vector<PoleMapy*> getPoleVOkoli(int x, int y);
public:
    static Mapa* getMapa(); // vrat instanci
    void ulozNaPozici(int x, int y, Zvire* zvire);
    void interakceZvirat();
    void pohybZvirat();
    void vypisMapu();
};


#endif //PROJEKT_MAPA_H
