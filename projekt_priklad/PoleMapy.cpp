//
// Created by Mikulas Muron on 08/04/2020.
//

#include "PoleMapy.h"



void PoleMapy::pridejZvire(Zvire *zvire) {
    m_zvirata.push_back(zvire);
}

void PoleMapy::setPoleVOkoli(std::vector<PoleMapy*> pole_v_okoli) {
    m_pole_v_okoli = pole_v_okoli;
}

void PoleMapy::interakceZvirat() {
    for(Zvire* zvire: m_zvirata){
        for(Zvire* zvire_k_sezrani: m_zvirata){
            zvire->snez(zvire_k_sezrani);
        }
    }
}

PoleMapy* PoleMapy::getNahodnePoleZOkoli() {
    // nahodne vyberu index
    int nahodny_index = rand() % m_pole_v_okoli.size();
    // vratim
    return m_pole_v_okoli[nahodny_index];
}

void PoleMapy::posunZvirata(std::vector<Zvire*> *jiz_posunuta_zvirata) {
    for(Zvire* zvire: m_zvirata){
        // nehybeme s mrtvyma zviratama
        if(! zvire->jeMrtve()){
            // nehybeme uz s jednou posunutyma zviratama
            if (std::find(jiz_posunuta_zvirata->begin(), jiz_posunuta_zvirata->end(), zvire) == jiz_posunuta_zvirata->end()){
                // posunu zvire na nahodne policko z okoli
                getNahodnePoleZOkoli()->pridejZvire(zvire);
                jiz_posunuta_zvirata->push_back(zvire);
                // smazu zvire z aktualniho policka
                m_zvirata.erase(std::find(m_zvirata.begin(), m_zvirata.end(), zvire));
            }
        }
    }
}

string PoleMapy::getZnacka() {
    string znacka = " ";
    if(obsahujeZvirata()){
        for(Zvire* zvire: m_zvirata) {
            znacka += zvire->getZnacka();
        }
    }
    else {
        znacka += "- ";
    }
    return znacka;
}


bool PoleMapy::obsahujeZvirata() {
    return m_zvirata.size() > 0;
}
