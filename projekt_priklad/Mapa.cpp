//
// Created by Mikulas Muron on 08/04/2020.
//

#include "Mapa.h"

Mapa::Mapa() {
    // projdeme 2d array a vytvorime PoleMapy
    for(int x = 0; x < m_mapa.size(); x++){
        for(int y = 0; y < m_mapa[x].size(); y++){
            m_mapa[x][y] = new PoleMapy();
        }
    }

    // nastavime kazdemu poli jeho okoli
    for(int x = 0; x < m_mapa.size(); x++){
        for(int y = 0; y < m_mapa[x].size(); y++){
            m_mapa[x][y]->setPoleVOkoli(getPoleVOkoli(x,y));
        }
    }

}

void Mapa::vypisMapu() {
    for(int x = 0; x < m_mapa.size(); x++){
        for(int y = 0; y < m_mapa[x].size(); y++){
            cout << m_mapa[x][y]->getZnacka() << "\t";
        }
        cout << endl;
    }
    cout << endl;
}


void Mapa::interakceZvirat() {
    for(int x = 0; x < m_mapa.size(); x++) {
        for (int y = 0; y < m_mapa[x].size(); y++) {
            m_mapa[x][y]->interakceZvirat();
        }
    }
}


void Mapa::ulozNaPozici(int x, int y, Zvire *zvire) {
    if(jeValidniSouradnice(x,y)) {
        m_mapa[x][y]->pridejZvire(zvire);
    }
}

Mapa* Mapa::getMapa() {
    // vytvorime jestli zatim nexistuje
    if(s_mapa == nullptr){
        s_mapa = new Mapa();
    }
    return s_mapa;
}

bool Mapa::jeValidniSouradnice(int x, int y) {
    return  x < m_mapa.size() &&
            x >= 0 &&
            y < m_mapa[x].size() &&
            y >= 0;
}



void Mapa::pohybZvirat() {
    std::vector<Zvire*> *posunuta_zvirata = new std::vector<Zvire*>();
    for(int x = 0; x < m_mapa.size(); x++) {
        for (int y = 0; y < m_mapa[x].size(); y++) {
            m_mapa[x][y]->posunZvirata(posunuta_zvirata);
        }
    }
}


std::vector<PoleMapy*> Mapa::getPoleVOkoli(int x, int y) {
    // TODO kontrola vstupu
    std::vector<PoleMapy*> policka;
    // projdu x-1 az x+1
    for(int x_okoli = x-1; x_okoli <= x+1; x_okoli++){
        // projdu y-1 az y+1
        for(int y_okoli = y-1; y_okoli <= y+1; y_okoli++) {
            // nedostal jsem se mimo meze mapy? nejsem na stejnem policku?
            if(jeValidniSouradnice(x_okoli,y_okoli) && ! (x_okoli == x && y_okoli == y)){
                policka.push_back(m_mapa[x_okoli][y_okoli]);
            }
        }
    }

    return policka;
}

Mapa* Mapa::s_mapa = nullptr;