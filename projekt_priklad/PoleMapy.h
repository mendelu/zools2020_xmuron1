//
// Created by Mikulas Muron on 08/04/2020.
//

#ifndef PROJEKT_POLEMAPY_H
#define PROJEKT_POLEMAPY_H

#include <vector>
#include <string>
#include <algorithm>
#include "Zvire.h"

class PoleMapy {
    // Tady toho muze byt v budoucnu vice nez jen zvire
    std::vector<Zvire*> m_zvirata;
    std::vector<PoleMapy*> m_pole_v_okoli;
    PoleMapy* getNahodnePoleZOkoli();
public:
    void setPoleVOkoli(std::vector<PoleMapy*> pole_v_okoli);
    bool obsahujeZvirata();
    void pridejZvire(Zvire* zvire);
    // Znacku pouzijeme ve vypisu mapy
    string getZnacka();
    void interakceZvirat();
    void posunZvirata(std::vector<Zvire*> *jiz_posunuta_zvirata);
};


#endif //PROJEKT_POLEMAPY_H
