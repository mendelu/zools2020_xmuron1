//
// Created by Mikulas Muron on 08/04/2020.
//

#ifndef PROJEKT_ZVIRE_H
#define PROJEKT_ZVIRE_H
#include <iostream>
using std::string;

class Zvire {
private:
    int m_hmotnost;
    string m_jmeno;
public:
    Zvire(string jmeno, int hmotnost);
    string getJmeno();
    int getHmotnost();
    void snez(Zvire* korist);
    void umri();
    bool jeMrtve();
    string getZnacka();

};

#endif //PROJEKT_ZVIRE_H
