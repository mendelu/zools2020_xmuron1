#include <iostream>

using namespace std;

class Automobil{
private:
    int m_zisk;
    int m_najeto;
    int m_cena_za_pujceni;

    int getCena(){
        int nova_cena = m_cena_za_pujceni;
        int pocet_slev = m_najeto / 10000;
        for(int i=0;i<pocet_slev;i++){
            nova_cena = nova_cena * 0.9;
        }
        return nova_cena;
    }

    void setZisk(int zisk){
        if(zisk < 0){
            m_zisk = 0;
        }
        else{
            m_zisk = zisk;
        }
    }

    void setNajeto(int najeto){
        const int maxNajeto = 250000;
        if ((najeto >= 0) and (najeto < maxNajeto)){
            m_najeto = najeto;
        }
        else{
            m_najeto = 0;
        }
    }

public:
    Automobil(int zisk,int najeto, int cena_za_pujceni){
        setZisk(zisk);
        setNajeto(najeto);
        m_cena_za_pujceni = cena_za_pujceni;
    }

    Automobil(int cena_za_pujceni){
        setZisk(0);
        setNajeto(0);
        m_cena_za_pujceni = cena_za_pujceni;
    }

    int getCenaZaPujceni(int pocet_dni){
        return pocet_dni * getCena();
    }

    void pridatZaznam(int pocet_dni, int najeto_km){
        m_najeto += najeto_km;
        m_zisk += getCenaZaPujceni(pocet_dni);
    }

    void printInfo(){
        cout << "Jsem Automobil" << endl;
        cout << "Vydelal jsem: " << m_zisk << endl;
        cout << "Najel jsem: " << m_najeto << endl;
        cout << "Na den stojim: " << getCena() << endl;
        cout << "Puvodni cena za den: " << m_cena_za_pujceni << endl;
    }
};

int main() {
    Automobil* skoda = new Automobil(0,0,100);
    skoda->pridatZaznam(10,51000);
    skoda->printInfo();

    Automobil* mazda = new Automobil(1000);
    mazda->pridatZaznam(1,500);
    mazda->pridatZaznam(2,1500);
    cout << mazda->getCenaZaPujceni(1) << endl;
    mazda->pridatZaznam(300,50000);
    mazda->printInfo();
    return 0;
}