//
// Created by Mikulas Muron on 19/04/2020.
//

#ifndef ZOO_CVICENI_VODA_H
#define ZOO_CVICENI_VODA_H

#include "Teren.h"
#include <iostream>

class Voda : public Teren {
private:
    int m_hloubka;

public:
    Voda(int skon, int hloubka);
    int getBonus();
    void printInfo();
    std::string getZnacka();

};


#endif //ZOO_CVICENI_VODA_H
