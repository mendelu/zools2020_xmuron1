#include <iostream>
using namespace std;

class Zidle {
public:
    string m_material;
    int m_nosnost;

    int getNosnost(){
        return m_nosnost;
    }

    void setNosnost(int novaNosnost){
        m_nosnost = novaNosnost;
    }

    string getMaterial(){
        return m_material;
    }

    void setMaterial(string novyMaterial){
        m_material = novyMaterial;
    }

    void printInfo(){
        cout << "Jsem zidle" << endl;
        cout << "Mam nosnost " << m_nosnost << endl;
        cout << "Jsem z materialu " << m_material << endl;
    }

};

int main() {
    Zidle* z1 = new Zidle();
    z1->setNosnost(10);
    z1->setMaterial("kov");
    z1->printInfo();
    z1->setMaterial("drevo");
    return 0;
}