#include <iostream>
using namespace std;

class Sportovec{
public:
    string m_jmeno;
    int m_celkem_km;

    Sportovec(){
        m_jmeno = "Bez jmena";
        m_pocet_km = 0;
    }

    string getJmeno(){
        return m_jmeno;
    }

    void setJmeno(string nove_jmeno){
        m_jmeno = nove_jmeno;
    }

    void behej(int kolik_km){
        m_celkem_km = m_celkem_km + kolik_km;
    }

    int getCelkemKm(){
        return m_celkem_km;
    }
};

int main() {
    Sportovec* s = new Sportovec();
    s->setJmeno("Pavel");
    s->behej(10);
    s->behej(20);
    cout << s->getCelkemKm() << endl;
    return 0;
}