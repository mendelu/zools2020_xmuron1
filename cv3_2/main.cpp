#include <iostream>
using namespace std;

// Dodat implementaci amortizace auta
// Cena se snizuje o 1.5 procenta za kadych 150 tis. km nad 100 tis.
// Upravit metodu registerRent(), aby vracela cenu za zapujcku

class Car{
    float m_coverInKm;
    float m_pricePerKm;
    float m_profit;

public:
    Car(float pricePerKm){
        setPricePerKm(pricePerKm);
        m_coverInKm = 0;
        m_profit = 0;
    }

    Car(float pricePerKm, float coverInKm){
        setPricePerKm(pricePerKm);
        setCoverInKm(coverInKm);
        m_profit = 0;
    }

    float getProfit(){
        return m_profit;
    }

    float getCoverInKm(){
        return m_coverInKm;
    }

    void registerRent(float coverPerRentInKm){
        /*
        // varianta 1
        bool isCoverOk = checkCoverPerRent(coverPerRentInKm);
        if (isCoverOk == true){
            //...
        }

        // varianta 2
        if (checkCoverPerRent(coverPerRentInKm) == true){
            //...
        }
        */

        // varianta 3: nema smysl testovat, zda je neco true
        if (checkCoverPerRent(coverPerRentInKm)){
            m_profit += calculateProfitPerRent(coverPerRentInKm);
            m_coverInKm += coverPerRentInKm;
        } else {
            cout << "Rent not registered!" << endl;
        }
    }

private:
    float calculateProfitPerRent(float coverPerRentInKm){
        float profitPerRent = m_pricePerKm*coverPerRentInKm;
        return  profitPerRent;
    }

    void setPricePerKm(float pricePerKm){
        const int maxPricePerKm = 1000;

        if ((pricePerKm > 0) and (pricePerKm < maxPricePerKm)){
            m_pricePerKm = pricePerKm;
        } else {
            m_pricePerKm = 0;
            cout << "Alert: Price per km must be (0," << maxPricePerKm << "). Now set on 0." << endl;
        }
    };

    bool checkCoverPerRent(float coverPerRentInKm){
        const int maxCoverPerRentInKm = 10000;
        /*
        if ((coverPerRentInKm >= 0) and (coverPerRentInKm < maxCoverPerRentInKm)){
            // OK
        } else {
            cout << "Alert: Cover per rent must be (0," << maxCoverPerRentInKm << ")." << endl;
        }
        */
        if ((coverPerRentInKm < 0) or (coverPerRentInKm >= maxCoverPerRentInKm)){
            cout << "Alert: Cover per rent must be (0," << maxCoverPerRentInKm << ")." << endl;
            return false;
        } else {
            return true;
        }
    };

    void setCoverInKm(float coverInKm){
        const int maxCoverInKm = 500000;

        if ((coverInKm >= 0) and (coverInKm < maxCoverInKm)){
            m_coverInKm = coverInKm;
        } else {
            m_coverInKm = 0;
            cout << "Alert: Cover must be (0,"<< maxCoverInKm << "). Now set on 0." << endl;
        }
    };
};

int main() {
    Car* fordMustang = new Car(10, 50000);
    fordMustang->registerRent(555);
    cout << "Cover:" << fordMustang->getCoverInKm() <<endl;
    cout << "Profit:" << fordMustang->getProfit() <<endl;


    delete fordMustang;
    return 0;
}
