#include <iostream>
#include <array>
using namespace std;

class Kontejner{
    float m_vaha;
    string m_obsah;
    string m_majitel;

public:
    Kontejner(float vaha, string obsah, string majitel){
        // TODO: kontrola vstupu
        m_vaha = vaha;
        m_majitel = majitel;
        m_obsah = obsah;
    }

    float getVaha(){
        return m_vaha;
    }

    string getObsah(){
        return m_obsah;
    }

    string getMajitel(){
        return m_majitel;
    }
};

class Patro{
    string m_oznaceni;
    array<Kontejner*, 10> m_pozice;
public:
    Patro(string oznaceni){
        m_oznaceni = oznaceni;
        // pozor na &, kdyz chci menit pole!!!
        for(Kontejner* &pozice:m_pozice){
            pozice = nullptr;
        }
    }

    void ulozKontejner(int pozice, Kontejner* kontejner){
        // TODO je pozice prazdna?
        if ( (pozice >= 0) and (pozice < 10) ){
            m_pozice.at(pozice) = kontejner;
        } else {
            cout << "Ukladas mimo rozsah pole pozic" << endl;
        }
    }

    void odeberKontejner(int pozice){
        if ( (pozice >= 0) and (pozice < 10) ){
            m_pozice.at(pozice) = nullptr;
        } else {
            cout << "Odebiras mimo rozsah pole pozic" << endl;
        }
    }

    void vypisObsah(){
        for(Kontejner* pozice:m_pozice) {
            if (pozice != nullptr) {
                cout << pozice->getObsah() << ": " << pozice->getVaha() << endl;
            } else {
                cout << "Pozice je prazdna" << endl;
            }
        }
    }
};


int main() {
    Kontejner* mobilniTelefony = new Kontejner(152.5, "Huawei", "Huawei PRC");
    Patro* p1 = new Patro("P1");
    p1->ulozKontejner(0, mobilniTelefony);
    p1->vypisObsah();
    p1->odeberKontejner(0);
    delete p1;
    delete mobilniTelefony;
    return 0;
}