//
// Created by Mikulas Muron on 06/04/2020.
//

#include "Kontejner.h"

Kontejner::Kontejner(string obsah, string majitel, int vaha) {
    m_obsah = obsah;
    m_majitel = majitel;
    m_vaha = vaha;
}

string Kontejner::getMajitel() {
    return m_majitel;
}

string Kontejner::getObsah() {
    return m_obsah;
}

int Kontejner::getVaha() {
    return m_vaha;
}

void Kontejner::printInfo() {
    std::cout << "Kontejner s obsahem " << m_obsah << " a vahou " << m_vaha << " a vlastni me " << m_majitel << std::endl;
}