//
// Created by Mikulas Muron on 06/04/2020.
//

#ifndef KONTEJNERY_SKLAD_H
#define KONTEJNERY_SKLAD_H

#include <iostream>
#include "Patro.h"

class Sklad {
    std::array<Patro*,5> m_patra;

public:
    Sklad();
    ~Sklad();
    void ulozDoSkladu(int patro, int pozice, Kontejner* kontejner);
    void odeberZeSkladu(int patro, int pozice);
    void printInfo();

};


#endif //KONTEJNERY_SKLAD_H
