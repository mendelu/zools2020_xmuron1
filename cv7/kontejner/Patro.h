//
// Created by Mikulas Muron on 06/04/2020.
//

#ifndef KONTEJNERY_PATRO_H
#define KONTEJNERY_PATRO_H

#include <iostream>
#include <array>
#include "Kontejner.h"
using std::string;

class Patro {
private:
    string m_oznaceni;
    std::array<Kontejner*,10> m_pozice;
public:
    Patro(string oznaceni);
    void pridej(int kam, Kontejner* kontejner);
    void odeber(int odkud);
    void printInfo();
};


#endif //KONTEJNERY_PATRO_H
