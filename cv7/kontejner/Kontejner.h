//
// Created by Mikulas Muron on 06/04/2020.
//

#ifndef KONTEJNERY_KONTEJNER_H
#define KONTEJNERY_KONTEJNER_H
#include <iostream>
using std::string;

class Kontejner {
private:
    string m_obsah;
    string m_majitel;
    int m_vaha;
public:
    Kontejner(string obsah, string majitel, int vaha);
    string getObsah();
    string getMajitel();
    int getVaha();
    void printInfo();
};


#endif //KONTEJNERY_KONTEJNER_H
