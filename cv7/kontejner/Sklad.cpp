//
// Created by Mikulas Muron on 06/04/2020.
//

#include "Sklad.h"

Sklad::Sklad() {
    // TODO dynamické pojmenování patra
    for(Patro* &patro : m_patra){
        patro = new Patro("Patro");
    }
}

Sklad::~Sklad() {
    for(Patro* &patro : m_patra){
        delete patro;
    }
}

void Sklad::ulozDoSkladu(int patro, int pozice, Kontejner *kontejner) {
    // TODO kontrola vstupu
    m_patra.at(patro)->pridej(pozice, kontejner);
}

void Sklad::odeberZeSkladu(int patro, int pozice) {
    // TODO kontrola vstupu
    m_patra.at(patro)->odeber(pozice);
}

void Sklad::printInfo() {
    for(Patro* patro: m_patra){
        if(patro != nullptr) {
            patro->printInfo();
        }
    }
}