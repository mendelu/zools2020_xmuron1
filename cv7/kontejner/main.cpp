#include <iostream>
#include "Sklad.h"

int main() {
    Kontejner* k1 = new Kontejner("telefony","Tomas",100);
    Kontejner* k2 = new Kontejner("cihly","Frantisek",1000);

    Sklad* sklad = new Sklad();
    sklad->ulozDoSkladu(0,0,k1);
    sklad->ulozDoSkladu(0,1,k2);
    sklad->printInfo();
    return 0;
}