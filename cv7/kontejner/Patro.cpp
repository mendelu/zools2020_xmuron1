//
// Created by Mikulas Muron on 06/04/2020.
//

#include "Patro.h"

Patro::Patro(string oznaceni) {
    m_oznaceni = oznaceni;
    for(Kontejner* &pozice : m_pozice){
        pozice = nullptr;
    }
}

void Patro::pridej(int kam, Kontejner *kontejner) {
    // TODO pridat kontrolu: je obsazena? je "kam" validni?
    m_pozice[kam] = kontejner;
}

void Patro::odeber(int odkud) {
    m_pozice[odkud] = nullptr;
}

void Patro::printInfo() {
    for(Kontejner* kontejner : m_pozice){
        if(kontejner != nullptr) {
            kontejner->printInfo();
        }
    }
}