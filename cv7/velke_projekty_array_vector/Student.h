//
// Created by Mikulas Muron on 05/04/2020.
//

#ifndef ZOO_VELKE_PROJEKTY_STUDENT_H
#define ZOO_VELKE_PROJEKTY_STUDENT_H

#include <iostream>

namespace IS{
class Student {
private:
    std::string m_jmeno;
public:
    Student(std::string jmeno);
    std::string getJmeno();
};
}


#endif //ZOO_VELKE_PROJEKTY_STUDENT_H
