#include <iostream>
#include <array>
#include <vector>
#include "Student.h"
//using IS::Student;


int main() {
    std::array<IS::Student*, 10> seznam;
    IS::Student* student1 = new IS::Student("Mikulas");
    IS::Student* student2 = new IS::Student("Pavel");
    seznam[0] = student1;
    seznam[1] = student2;
    std::cout << seznam[0]->getJmeno() << std::endl;
    std::cout << seznam.at(1)->getJmeno() << std::endl;

    std::vector<IS::Student*> seznam2;
    seznam2.push_back(student1);
    seznam2.push_back(student2);
    seznam2.pop_back();
    seznam2.back()->getJmeno();
    std::cout << seznam2.size() << std::endl;





    return 0;
}