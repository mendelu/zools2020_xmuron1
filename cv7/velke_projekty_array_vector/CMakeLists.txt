cmake_minimum_required(VERSION 3.15)
project(zoo_velke_projekty)

set(CMAKE_CXX_STANDARD 14)

add_executable(zoo_velke_projekty main.cpp Student.cpp Student.h)