#include <iostream>
using namespace std;

class Hrdina{
private:
    string m_jmeno;
    static int m_pocet;
public:
    Hrdina(string jmeno){
        m_jmeno = jmeno;
        m_pocet += 1;
    }

    ~Hrdina(){
        m_pocet -= 1;
    }

    static int getVelikostDruziny(){
        return Hrdina::m_pocet;
    }

};

int Hrdina::m_pocet = 0;

int main() {
    Hrdina* h1 = new Hrdina("David");
    Hrdina* h2 = new Hrdina("Mikulas");
    cout << Hrdina::getVelikostDruziny() << endl;
    delete h1;
    cout << Hrdina::getVelikostDruziny() << endl;
    return 0;
}